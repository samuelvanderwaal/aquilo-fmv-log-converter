"""
Convert DJI Phantom 3 Log files to a specific CSV format to be used with FMV
Sam Vanderwaal
Aquilo, 2016
"""
from math import *
from shutil import copyfile
from datetime import datetime
import os

# _____DJI Log Field Mapping_____

# Converted P3 Log File Value         |   MISB Value
#                                     |
# CUSTOM.updateTime                   |   UNIX Time Stamp
# OSD.latitude                        |   Sensor Latitude
# OSD.longitude                       |   Sensor longitude
# OSD.height + AMSL                   |   Sensor True Altitude
# OSD.pitch                           |   Platform Pitch Angle
# OSD.roll                            |   Platform Roll Angle
# OSD.yawCompass                      |   Platform Heading Angle
# GIMBAL.yawCompass - OSD.yawCompass  |   Sensor Relative Azimuth Angle
# GIMBAL.pitch                        |   Sensor Relative Elevation Angle
# GIMBAL.roll                         |   Sensor Relative Roll Angle


template_file = 'template.csv'


# def converter(log_file_name, out_file_name, sensor_h_fov, sensor_v_fov,
#               mission_id, plat_tail_num, plat_desig, img_sensor,
#               img_coord_sys, amsl):
def converter(log_file_name, out_file_name, sensor_h_fov, sensor_v_fov, amsl):
    # Indexes
    # CHECKSUM is index 0 and must be the first column
    # CHECKSUM can be populated with zeros or empty, but must
    # be present as a column
    TIMESTAMP = 1
    MISSION_ID = 2
    PLAT_TAIL_NUM = 3
    PLAT_HEADING_ANG = 4
    PLAT_PITCH_ANG = 5
    PLAT_ROLL_ANG = 6
    PLAT_DESIGNATION = 7
    IMG_SENSOR = 8
    IMG_COORD_SYS = 9
    SENSOR_LAT = 10
    SENSOR_LON = 11
    SENSOR_TRUE_ALT = 12
    SENSOR_H_FOV = 13
    SENSOR_V_FOV = 14
    SENSOR_AZ_ANG = 15
    SENSOR_EL_ANG = 16
    SENSOR_ROLL_ANG = 17

    # MISB List Length, change this value if more columns are added
    MISB_LEN = 18

    # Only twenty values in the "green" subset
    # ArcGIS calculates the other necessary values from this subset
    misb_list = [0]*MISB_LEN

    name_map = {'CUSTOM.updateTime': 'UNIX Time Stamp',
                'OSD.latitude': 'Sensor Latitude',
                'OSD.longitude': 'Sensor Longitude',
                'OSD.height': 'AGL Altitude',
                'OSD.pitch': 'Platform Pitch Angle',
                'OSD.roll': 'Platform Roll Angle',
                'OSD.yawCompass': 'Platform Heading Angle',
                'GIMBAL.yawCompass': 'Sensor Abs Azimuth Angle',
                'GIMBAL.pitch': 'Sensor Relative Elevation Angle',
                'GIMBAL.roll': 'Sensor Relative Roll Angle'}

    misb_positions = {}

    # # Get log file names
    # log_file_name = input("Enter the full name of the DJI Log file: ")
    # template_file = input("Enter the full name of the template to use: ")
    # out_file_name = input("Enter name of the output file: ")

    log_file = open(log_file_name, 'r')
    copyfile(template_file, out_file_name)
    out_file = open(out_file_name, 'r+')

    # Create list of DJI header names
    header_list = log_file.readline().split(',')

    # Look through header list for headers in our name map dictionary
    for header in header_list:
        # If the header is in our map, get it's column position
        # and add a key value pair to the misb_positions dictionary
        # misb_value : column position
        if header in name_map:
            misb_value = name_map[header]
            header_index = header_list.index(header)
            misb_positions[misb_value] = header_index

    # Skip header line for output file
    next(out_file)

    # Iterate through DJI log file
    for line in log_file:
        # Split line by commas
        line_list = line.split(',')

        # Get DJI local datetime and convert to UNIX timestamp
        date = line_list[misb_positions['UNIX Time Stamp']]
        unix_timestamp = datetime.strptime(date,
                                           "%Y-%m-%d %H:%M:%S.%f").timestamp()
        misb_timestamp = int(unix_timestamp * 1000000)

        # Aircraft values
        sensor_lat = float(line_list[misb_positions['Sensor Latitude']])
        sensor_lon = float(line_list[misb_positions['Sensor Longitude']])
        sensor_true_alt = float(line_list[misb_positions[
                            'AGL Altitude']]) + float(amsl)
        plat_pitch_ang = float(line_list[misb_positions[
                            'Platform Pitch Angle']])
        plat_roll_ang = float(line_list[misb_positions['Platform Roll Angle']])
        plat_heading_ang = float(line_list[misb_positions[
                            'Platform Heading Angle']])

        # Gimbal values
        sensor_el_ang = float(line_list[misb_positions[
                            'Sensor Relative Elevation Angle']])
        sensor_roll_ang = float(line_list[misb_positions[
                                'Sensor Relative Roll Angle']])
        sensor_heading = float(line_list[misb_positions[
                                'Sensor Abs Azimuth Angle']])
        sensor_az_ang = sensor_heading - plat_heading_ang

        # Use defined tag indexes and create a list of MISB tags
        misb_list[TIMESTAMP] = misb_timestamp
        # misb_list[MISSION_ID] = mission_id
        # misb_list[PLAT_TAIL_NUM] = plat_tail_num
        misb_list[PLAT_HEADING_ANG] = plat_heading_ang
        misb_list[PLAT_PITCH_ANG] = plat_pitch_ang
        misb_list[PLAT_ROLL_ANG] = 0
        # misb_list[PLAT_ROLL_ANG] = plat_roll_ang
        # misb_list[PLAT_DESIGNATION] = plat_desig
        # misb_list[IMG_SENSOR] = img_sensor
        # misb_list[IMG_COORD_SYS] = img_coord_sys
        misb_list[SENSOR_LAT] = sensor_lat
        misb_list[SENSOR_LON] = sensor_lon
        misb_list[SENSOR_TRUE_ALT] = sensor_true_alt
        misb_list[SENSOR_H_FOV] = sensor_h_fov
        misb_list[SENSOR_V_FOV] = sensor_v_fov
        misb_list[SENSOR_AZ_ANG] = sensor_az_ang
        misb_list[SENSOR_EL_ANG] = sensor_el_ang
        # misb_list[SENSOR_ROLL_ANG] = sensor_roll_ang
        misb_list[SENSOR_ROLL_ANG] = 0

        # Iterate through misb_list
        # write no data for zeros, otherwise write klv tags
        # for element in misb_list:
        #     if (element == 0):
        #         out_file.write(',')
        #     else:
        #         out_file.write(str(element) + ',')

        for i in range(0, MISB_LEN):
            if i == 0:
                out_file.write(',')
            else:
                out_file.write(str(misb_list[i]) + ',')

        out_file.write('\n')

    log_file.close()
    out_file.close()

if __name__ == '__main__':
    log_file_name = eval(input("Log file: "))
    out_file_name = eval(input("Name of output file: "))
    fov_h = input("Horizontal field of view: ")
    fov_v = input("Vertical field of view: ")
    amsl = input("Enter average amsl: ")

    log_file_path = os.path.normpath(log_file_name)
    out_file_path = os.path.normpath(out_file_name)
    converter(log_file_name, out_file_name, fov_h, fov_v, amsl)
